### Keybase proof

I hereby claim:

  * I am di on github.
  * I am dustingram (https://keybase.io/dustingram) on keybase.
  * I have a public key whose fingerprint is 321D E2BA 9BA7 5C06 AF95  53F6 93D2 B8D4 930A 5E39

To claim this, I am signing this object:

```json
{
    "body": {
        "key": {
            "fingerprint": "321de2ba9ba75c06af9553f693d2b8d4930a5e39",
            "host": "keybase.io",
            "key_id": "93d2b8d4930a5e39",
            "kid": "0101b62b7bab833c31733397920cc103ac91f9bd3bdc9ce6dc641885a27c7416a1090a",
            "uid": "16f02f0c1e858a4a741cfe9819cc7b19",
            "username": "dustingram"
        },
        "service": {
            "name": "github",
            "username": "di"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1436456648,
    "expire_in": 157680000,
    "prev": "1f2ae5c0d28bc86cff333f8295535ca9fa2dbbb7d0e5a28870edad0f53a7b851",
    "seqno": 2,
    "tag": "signature"
}
```

with the key [321D E2BA 9BA7 5C06 AF95  53F6 93D2 B8D4 930A 5E39](https://keybase.io/dustingram), yielding the signature:

```
-----BEGIN PGP MESSAGE-----
Version: Keybase OpenPGP v2.0.20
Comment: https://keybase.io/crypto

yMMqAnicbVJ7UFRVHN5lVR6NaJJClo8ukmNs633sfa3MECkpTlogrIMRdM695+5e
N3e3ffCQsJC0wrRkJXkY6zBBaMlEzIAWQZZuDDhCDi5MhCnQbGOJRklaOHQvY/80
nX/O/L7f933n/L5zzs3XaaK0vkunB3xRuby29yzt1WTVvd9djEGHWISZijEbmt0k
2W5BLqdLtnswE0aRhIhICHgIWFrAGSDxNE1JDE+JJOREI0/hgEYUj+kxq8OtKhQb
CNzIIDsUTCnyZFFB/4dvm23gBE5AhoQsBJCjKIEiWIqieJYncUEgcAoIPCHxUKSg
KPACYkSBMRIcRwOSFVgjwQAC53Gg2Hln7QhGwkkJFwjE0RwwAoUiSIjnCF4QWEio
53rdyGUHu5DCFr1ujzKuC+zCSvSYgufLAlJDuN+3yB6rF/5HI6tcT5FTLQoQzLsv
y4OyXVTMFHY+crllhx0zEQpT8MiqjjBSjJFmGCOnx1ChU3ahPFll0CzD4crSY04X
ylcnkEiAlKhFkoMCxwiSpOQhcaSaOy0AXgKkCCFkRRwpIXAciyMRiLhEU4CFHE1g
6iCv2B2YiVSuCSyKpVu22IHH60JYyddnc+ZotFGaeXPD1NfXREUu/PdP3OUXzGRu
iknsHG2+lTHwRf9N3xv1U7dfZWunVsd9aQzr83Q+fdnvMveODe9rbYuM1HZwocbg
yKrBJV0nIjdHVA9mN54D499l7ilMW2woaavtDNSsuzWet7PyaHXFWjZ+SUIwub+j
tXi/PDzSPQKaT9dFB2zvFp9KjWx/Mifn2wzrwN+lNf2x8dEtWyuuwZg/l5YPr//4
nfO+7Ycjjg6nGMbiuq7qKjznC8Je2O5cuzfw2JaerIvz5vZZVwdGX7qGZ/aQ98Z7
H5jZPfG5rsz94ZGqHw6sMAeT5ySkNjVHD4121h/MurfTE18rHtKb0heJjWNp32ye
yNZvWLG8/uQvDvHg9UKtoeNGTcNzG/5CL65pw47x1E+6ocQC6x8P/7gxKz22inCk
PD+9Y33Tytiyya3560YXhZsZcLf5tVxX4E7pfE1l6o7uD74vj3iKuvh48lJyHxtq
Pdlf5Y+bPm678ulXZStLu8a8pusjN0qYTeUtubC6LLwLNk1FtG+ZtjRU2z4zHxkK
1D7qPpNwxmTbuypjYmFTot9Lhtfo/HWi2D/9ns0aXHAHf9lZeUIq+kjo8Sc98foj
k289tAy0vr2nx/Dscu0nVzsupx3blnYhlNiYfHx3aGa/EHNpWcNvBw7PPJjeku3G
k3xTwZtmkv4dJCc4kq6kvBkaxDP76FjLtoLJxb/+PP7Mqck1ty+0/wMXE6ha
=TcSj
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/dustingram

### From the command line:

Consider the [keybase command line program](https://keybase.io/docs/command_line).

```bash
# look me up
keybase id dustingram

# encrypt a message to me
keybase encrypt dustingram -m 'a secret message...'

# ...and more...
```
